# frozen_string_literal: true

# Defines a module named Recipes
module Recipes
  # Defines a new class called Client
  class Client
    # Loads the file recipes/pages.rb
    require_relative 'pages'

    def self.recipes
      # Set the variable recipes to the value returned by running the function
      # read_from_files. It utilizes class instance variables to cache the
      # retrieved data for efficiency, so subsequent calls to this method will
      # return the cached data if available.
      @recipes ||= read_from_files
    end

    def self.read_from_files
      # Set the variable objects to an empty Array
      objects = []
      # Loop over the files matching a pattern
      Dir['data/recipes/*.yaml'].each do |file|
        # Add an item to the variable objects determined by reading the contents
        # of a file using YAML and convert it into a ruby object
        objects.push(YAML.safe_load_file(file))
      end
      # Return the value of the variable objects
      objects
    end

    def self.renderer(page)
      # It reads the content of an ERB file located in the data/templates
      # directory corresponding to the given page name, compiles it into an ERB
      # template using ERB.new, and returns the template object.
      ERB.new(File.read("data/templates/#{page}.erb"))
    end
  end
end
