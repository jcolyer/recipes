# frozen_string_literal: true

# Defines a module named Recipes
module Recipes
  # Loads the Bundler setup from the Gemfile
  require 'bundler/setup'
  # Tells Bundler to load all the gems specified in the Gemfile
  Bundler.require(:default)
  # Loads the file recipes/client.rb
  require_relative 'recipes/client'
end
